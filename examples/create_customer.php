<?php
use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Customer;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$customer = new Customer();
$customer->setName('Test Customer');
$customer->setResponsibleUserId($account->getCurrentUserId());
$customer->setCreatedBy($account->getCurrentUserId());
$customer->setNextDate(strtotime('+1 week'));
$customer->setPeriodicity(7);
$customer->setNextPrice(1000);

$api->addCustomer($customer);
