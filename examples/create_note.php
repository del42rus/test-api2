<?php

use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Note;
use Amocrm\ApiClient\Entity\Contact;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$contact = new Contact();
$contact->setName('Jane Doe');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());

$api->addContact($contact);

$note = new Note();
$note->setElementType(Note::ENTITY_CONTACT);
$note->setElementId($contact->getId());
$note->setNoteType(Note::TYPE_COMMON);
$note->setText('Some Note Text');
$note->setResponsibleUserId($account->getCurrentUserId());

$api->addNote($note);

$note = new Note();
$note->setElementType(Note::ENTITY_CONTACT);
$note->setElementId($contact->getId());
$note->setNoteType(Note::TYPE_SMS_IN);
$note->setParams(['text' => 'Some SMS Text']);
$note->setResponsibleUserId($account->getCurrentUserId());

$api->addNote($note);
