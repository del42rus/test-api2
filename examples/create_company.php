<?php
use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Company;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$company = new Company();
$company->setName('Test Company');
$company->setResponsibleUserId($account->getCurrentUserId());

$api->addCompany($company);
