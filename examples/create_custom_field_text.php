<?php

use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\CustomField;
use Amocrm\ApiClient\Entity\Contact;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$customField = new CustomField();
$customField->setName('Custom Text Field 1');
$customField->setFieldType(CustomField::TYPE_TEXT);
$customField->setElementType(CustomField::ENTITY_CONTACT);
$customField->setOrigin('528d0285c1f9180911159a9dc6f759b3_zendesk_widget');
$customField->setIsEditable(true);
$customField->setIsVisible(true);

$api->addCustomField($customField);

$contact = new Contact();
$contact->setName('Tom Hardy');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());
$contact->addCustomField([
    'id' => $customField->getId(),
    'values' => [
        ['value' => 'Text Value']
    ]
]);

$api->addContact($contact);
