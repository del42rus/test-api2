<?php

use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\CustomField;
use Amocrm\ApiClient\Entity\Contact;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$customField = new CustomField();
$customField->setName('Multiselect 1');
$customField->setFieldType(CustomField::TYPE_MULTISELECT);

$enums = [];

for ($i = 1; $i <= 10; $i++) {
    $enums[] = 'Value ' . $i;
}

$customField->setEnums($enums);
$customField->setElementType(CustomField::ENTITY_CONTACT);
$customField->setOrigin('528d0285c1f9180911159a9dc6f759b3_zendesk_widget');
$customField->setIsEditable(false);
$customField->setIsVisible(true);

$api->addCustomField($customField);

$account = $api->getAccount(['with' => 'custom_fields']);

$customFields = $account->getCustomFields();
$customField = $customFields['contacts'][$customField->getId()];

$contact = new Contact();
$contact->setName('Bob Marley');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());

$enumsId = array_keys($customField->getEnums());
shuffle($enumsId);

$contact->addCustomField([
    'id' => $customField->getId(),
    'values' => array_splice($enumsId, 0, mt_rand(1, count($enumsId) - 1))
]);

$api->addContact($contact);
