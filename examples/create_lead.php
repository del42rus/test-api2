<?php
use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Lead;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$lead = new Lead();
$lead->setName('New Lead');
$lead->setResponsibleUserId($account->getCurrentUserId());
$lead->setSale(1000);

$api->addLead($lead);
