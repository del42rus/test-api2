<?php

use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Task;
use Amocrm\ApiClient\Entity\Contact;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$contact = new Contact();
$contact->setName('Will Smith');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());

$api->addContact($contact);

$task = new Task();
$task->setElementType(Task::ENTITY_CONTACT);
$task->setElementId($contact->getId());
$task->setTaskType(Task::TYPE_CALL);
$task->setCompleteTillAt(strtotime('+ 1 day'));
$task->setText('Call tomorrow');
$task->setResponsibleUserId($account->getCurrentUserId());
$task->setCreatedBy($account->getCurrentUserId());

$api->addTask($task);

$task->setIsCompleted(true);

$api->updateTask($task);
