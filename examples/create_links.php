<?php

use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Company;
use Amocrm\ApiClient\Entity\Contact;
use Amocrm\ApiClient\Entity\Lead;
use Amocrm\ApiClient\Entity\Customer;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$company = new Company();
$company->setName('Test Company');
$company->setResponsibleUserId($account->getCurrentUserId());

$api->addCompany($company);

$contact = new Contact();
$contact->setName('Test Contact');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());
$contact->setCompanyId($company->getId());

$api->addContact($contact);

$lead = new Lead();
$lead->setName('Test Lead');
$lead->setResponsibleUserId($account->getCurrentUserId());
$lead->setSale(1000);
$lead->setCompanyId($company->getId());
$lead->addContactId($contact->getId());

$api->addLead($lead);

$customer = new Customer();
$customer->setName('Test Customer');
$customer->setResponsibleUserId($account->getCurrentUserId());
$customer->setCreatedBy($account->getCurrentUserId());
$customer->setNextDate(strtotime('+1 week'));
$customer->setPeriodicity(7);
$customer->setNextPrice(1000);
$customer->setCompanyId($company->getId());
$customer->addContactId($contact->getId());

$api->addCustomer($customer);
