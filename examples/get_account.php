<?php
use Amocrm\ApiClient;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount(['with' => 'custom_fields']);

var_dump($account);
