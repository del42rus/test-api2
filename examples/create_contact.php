<?php
use Amocrm\ApiClient;
use Amocrm\ApiClient\Entity\Contact;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config.php';

$api = ApiClient\ClientFactory::create($config);

$account = $api->getAccount();

$contact = new Contact();
$contact->setName('John Doe');
$contact->setResponsibleUserId($account->getCurrentUserId());
$contact->setCreatedBy($account->getCurrentUserId());

$api->addContact($contact);
