<?php

namespace Amocrm\ApiClient\Entity;


class Company extends AbstractEntity
{
    protected $name;

    protected $responsibleUserId;

    protected $createdBy;

    protected $contactsId = [];

    protected $leadsId = [];

    protected $customersId = [];

    protected $customFields = [];

    protected $createdAt;

    protected $updatedAt;

    public function __construct()
    {
        $this->createdAt = time();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getResponsibleUserId()
    {
        return $this->responsibleUserId;
    }

    public function setResponsibleUserId($responsibleUserId)
    {
        $this->responsibleUserId = $responsibleUserId;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getContactsId()
    {
        return $this->contactsId;
    }

    public function setContactsId($contactsId)
    {
        $this->contactsId = $contactsId;
    }

    public function addContactId($contactId)
    {
        $key = array_search($contactId, $this->contactsId);

        if ($key) {
            return;
        }

        $this->contactsId[] = $contactId;
    }

    public function getLeadsId()
    {
        return $this->leadsId;
    }

    public function setLeadsId($leadsId)
    {
        $this->leadsId = $leadsId;
    }

    public function addLeadId($leadId)
    {
        $key = array_search($leadId, $this->leadsId, true);

        if ($key) {
            return;
        }

        $this->leadsId[] = $leadId;
    }

    public function getCustomersId()
    {
        return $this->customersId;
    }

    public function setCustomersId($customersId)
    {
        $this->customersId = $customersId;
    }

    public function addCustomerId($customerId)
    {
        $key = array_search($customerId, $this->customers, true);

        if ($key) {
            return;
        }

        $this->customersId[] = $customerId;
    }

    public function getCustomFields()
    {
        return $this->customFields = [];
    }

    public function setCustomFields(array $customFields)
    {
        $this->customFields = $customFields;
    }

    public function addCustomField($customField)
    {
        $this->customFields[] = $customField;
    }
}
