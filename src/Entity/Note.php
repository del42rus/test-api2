<?php

namespace Amocrm\ApiClient\Entity;

class Note extends AbstractEntity
{
    const TYPE_COMMON = 4;
    const TYPE_TASK_RESULT = 13;
    const TYPE_SYSTEM = 25;
    const TYPE_SMS_IN = 102;
    const TYPE_SMS_OUT = 103;

    const ENTITY_CONTACT = 1;
    const ENTITY_LEAD = 2;
    const ENTITY_COMPANY = 3;
    const ENTITY_TASK = 4;
    const ENTITY_CUSTOMER = 12;

    protected $text;

    protected $elementId;

    protected $elementType;

    protected $noteType;

    protected $responsibleUserId;

    protected $params;

    protected $createdAt;

    protected $updatedAt;

    public function __construct()
    {
        $this->createdAt = time();
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function setNoteType($noteType)
    {
        $this->noteType = $noteType;
    }

    public function getNoteType()
    {
        return $this->noteType;
    }

    public function setElementType($elementType)
    {
        $this->elementType = $elementType;
    }

    public function getElementId()
    {
        return $this->elementId;
    }

    public function setElementId($elementId)
    {
        $this->elementId = $elementId;
    }

    public function getElementType()
    {
        return $this->elementType;
    }

    public function getResponsibleUserId()
    {
        return $this->responsibleUserId;
    }

    public function setResponsibleUserId($responsibleUserId)
    {
        $this->responsibleUserId = $responsibleUserId;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }
}
