<?php

namespace Amocrm\ApiClient\Entity;

class Account extends AbstractEntity
{
    protected $name;

    protected $currentUserId;

    protected $customFields = [];

    public function __construct()
    {
        $this->customFields['contacts'] = [];
        $this->customFields['companies'] = [];
        $this->customFields['leads'] = [];
        $this->customFields['customers'] = [];
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCurrentUserId()
    {
        return $this->currentUserId;
    }

    public function setCurrentUserId($currentUserId)
    {
        $this->currentUserId = $currentUserId;
    }

    public function getCustomFields()
    {
        return $this->customFields;
    }

    public function setCustomFields($customFields)
    {
        $this->customFields = $customFields;
    }

    public function addCustomField(CustomField $customField)
    {
        switch ($customField->getElementType()) {
            case CustomField::ENTITY_CONTACT:
                $this->customFields['contacts'][$customField->getId()] = $customField;
                break;
            case CustomField::ENTITY_COMPANY:
                $this->customFields['companies'][$customField->getId()] = $customField;
                break;
            case CustomField::ENTITY_LEAD:
                $this->customFields['leads'][$customField->getId()] = $customField;
                break;
            case CustomField::ENTITY_CUSTOMER:
                $this->customFields['customers'][$customField->getId()] = $customField;
                break;
            case CustomField::ENTITY_CATALOG:
                $this->customFields['catalogs'][$customField->getId()] = $customField;
                break;
        }
    }
}
