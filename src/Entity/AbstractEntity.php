<?php

namespace Amocrm\ApiClient\Entity;

class AbstractEntity
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function populate(array $data)
    {
        foreach ($data as $field => $value) {
            $field = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $field))));

            if (!property_exists($this, $field)) {
                continue;
            }

            $setter = 'set' . ucfirst($field);

            if (!method_exists($this, $setter)) {
                continue;
            }

            $this->$setter($value);
        }
    }

    public function getArrayCopy()
    {
        $vars = get_object_vars($this);

        $data = [];

        foreach ($vars as $key => $value) {
            $data[strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $key))] = $value;
        }

        return $data;
    }
}
