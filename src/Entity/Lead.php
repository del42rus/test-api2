<?php

namespace Amocrm\ApiClient\Entity;

class Lead extends AbstractEntity
{
    protected $name;

    protected $sale;

    protected $responsibleUserId;

    protected $contactsId = [];

    protected $companyId;

    protected $createdAt;

    protected $updatedAt;

    protected $customFields = [];

    public function __construct()
    {
        $this->createdAt = time();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    public function getSale()
    {
        return $this->sale;
    }

    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    public function getResponsibleUserId()
    {
        return $this->responsibleUserId;
    }

    public function setResponsibleUserId($responsibleUserId)
    {
        $this->responsibleUserId = $responsibleUserId;
    }

    public function getContactsId()
    {
        return $this->contactsId;
    }

    public function setContactsId($contactsId)
    {
        $this->contactsId = $contactsId;
    }

    public function addContactId($contactId)
    {
        $key = array_search($contactId, $this->contactsId);

        if ($key) {
            return;
        }

        $this->contactsId[] = $contactId;
    }

    public function getCustomFields()
    {
        return $this->customFields = [];
    }

    public function setCustomFields(array $customFields)
    {
        $this->customFields = $customFields;
    }

    public function addCustomField($customField)
    {
        $this->customFields[] = $customField;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
