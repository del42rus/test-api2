<?php

namespace Amocrm\ApiClient\Entity;

class CustomField extends AbstractEntity
{
    const TYPE_TEXT = 1;
    const TYPE_NUMERIC = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_SELECT = 4;
    const TYPE_MULTISELECT = 5;
    const TYPE_DATE = 6;
    const TYPE_URL = 7;
    const TYPE_MULTITEXT = 8;
    const TYPE_TEXTAREA = 9;
    const TYPE_RADIOBUTTON = 10;
    const TYPE_STREETADDRESS = 11;
    const TYPE_SMARTADDRESS = 13;
    const TYPE_BIRTHDAY = 14;
    const TYPE_LEGAL_ENTITY = 15;
    const TYPE_ITEMS = 16;

    const ENTITY_CONTACT = 1;
    const ENTITY_LEAD = 2;
    const ENTITY_COMPANY = 3;
    const ENTITY_CUSTOMER = 12;
    const ENTITY_CATALOG = 10;
    
    protected $name;

    protected $fieldType;

    protected $elementType;

    protected $origin;

    protected $isEditable;

    protected $isRequired;

    protected $isDeletable;

    protected $isVisible;

    protected $enums;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setFieldType($fieldType)
    {
        $this->fieldType = $fieldType;
    }

    public function getFieldType()
    {
        return $this->fieldType;
    }

    public function setElementType($elementType)
    {
        $this->elementType = $elementType;
    }

    public function getElementType()
    {
        return $this->elementType;
    }

    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    public function getOrigin()
    {
        return $this->origin;
    }

    public function setIsEditable($isEditable)
    {
        $this->isEditable = $isEditable;
    }

    public function getIsEditable()
    {
        return $this->isEditable;
    }

    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;
    }

    public function getIsRequired()
    {
        return $this->isRequired;
    }

    public function setIsDeletable($isDeletable)
    {
        $this->isDeletable = $isDeletable;
    }

    public function getIsDeletable()
    {
        return $this->isDeletable;
    }

    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    public function getIsVisible()
    {
        return $this->isVisible;
    }

    public function getEnums()
    {
        return $this->enums;
    }

    public function setEnums(array $enums)
    {
        $this->enums = $enums;
    }
}
