<?php

namespace Amocrm\ApiClient\Entity;

class Task extends AbstractEntity
{
    const TYPE_CALL = 1;
    const TYPE_APPOINTMENT = 2;
    const TYPE_WRITE_EMAIL = 3;

    const ENTITY_CONTACT = 1;
    const ENTITY_LEAD = 2;
    const ENTITY_COMPANY = 3;
    const ENTITY_CUSTOMER = 12;

    protected $text;

    protected $elementId;

    protected $elementType;

    protected $taskType;

    protected $responsibleUserId;

    protected $createdBy;

    protected $createdAt;

    protected $updatedAt;

    protected $completeTillAt;

    protected $isCompleted;

    public function __construct()
    {
        $this->createdAt = time();
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function setTaskType($taskType)
    {
        $this->taskType = $taskType;
    }

    public function getTaskType()
    {
        return $this->taskType;
    }

    public function setElementType($elementType)
    {
        $this->elementType = $elementType;
    }

    public function getElementId()
    {
        return $this->elementId;
    }

    public function setElementId($elementId)
    {
        $this->elementId = $elementId;
    }

    public function getElementType()
    {
        return $this->elementType;
    }

    public function getResponsibleUserId()
    {
        return $this->responsibleUserId;
    }

    public function setResponsibleUserId($responsibleUserId)
    {
        $this->responsibleUserId = $responsibleUserId;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCompleteTillAt($completeTillAt)
    {
        $this->completeTillAt = $completeTillAt;
    }

    public function getCompleteTillAt()
    {
        return $this->completeTillAt;
    }

    public function setIsCompleted($isCompleted)
    {
        $this->isCompleted = $isCompleted;
    }

    public function getIsCompleted()
    {
        return $this->isCompleted;
    }
}
