<?php

namespace Amocrm\ApiClient\Entity;

class Customer extends AbstractEntity
{
    protected $name;

    protected $nextDate;

    protected $nextPrice;

    protected $responsibleUserId;

    protected $createdBy;

    protected $periodicity;

    protected $companyId;

    protected $contactsId = [];

    protected $customFields = [];

    protected $createdAt;

    protected $updatedAt;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getNextDate()
    {
        return $this->nextDate;
    }

    public function setNextDate($nextDate)
    {
        $this->nextDate = $nextDate;
    }

    public function getNextPrice()
    {
        return $this->nextPrice;
    }

    public function setNextPrice($nextPrice)
    {
        $this->nextPrice = $nextPrice;
    }

    public function getResponsibleUserId()
    {
        return $this->responsibleUserId;
    }

    public function setResponsibleUserId($responsibleUserId)
    {
        $this->responsibleUserId = $responsibleUserId;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    public function getContactsId()
    {
        return $this->contactsId;
    }

    public function setContactsId($contactsId)
    {
        $this->contactsId = $contactsId;
    }

    public function addContactId($contactId)
    {
        $key = array_search($contactId, $this->contactsId);

        if ($key) {
            return;
        }

        $this->contactsId[] = $contactId;
    }

    public function getCustomFields()
    {
        return $this->customFields = [];
    }

    public function setCustomFields(array $customFields)
    {
        $this->customFields = $customFields;
    }

    public function addCustomField($customField)
    {
        $this->customFields[] = $customField;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
