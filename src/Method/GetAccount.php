<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Entity\Account;
use Amocrm\ApiClient\Entity\CustomField;

class GetAccount extends AbstractMethod
{
    public function __invoke($params = [])
    {
        $response = $this->client->get('/api/v2/account', $params);

        $account = new Account();

        $account->setId($response->id);
        $account->setName($response->name);
        $account->setCurrentUserId($response->current_user);

        if (isset($response->_embedded) &&
            isset($response->_embedded->custom_fields)) {
            $this->setAccountCustomFields($account, $response->_embedded->custom_fields);
        }

        return $account;
    }

    private function setAccountCustomFields($account, $customFields)
    {
        foreach ($customFields as $elementType => $elementCustomFields) {
            if ($elementType == 'catalogs') {
                $tmp = [];
                foreach ($elementCustomFields as $key => $items) {
                    foreach ($items as $id => $field) {
                        $tmp[$id] = $field;
                    }
                }

                $elementCustomFields = $tmp;
            }

            foreach ($elementCustomFields as $id => $field) {
                $customField = new CustomField();

                switch ($elementType) {
                    case 'contacts':
                        $customField->setElementType(CustomField::ENTITY_CONTACT);
                        break;
                    case 'companies':
                        $customField->setElementType(CustomField::ENTITY_COMPANY);
                        break;
                    case 'leads':
                        $customField->setElementType(CustomField::ENTITY_LEAD);
                        break;
                    case 'customers':
                        $customField->setElementType(CustomField::ENTITY_CUSTOMER);
                        break;
                    case 'catalogs':
                        $customField->setElementType(CustomField::ENTITY_CATALOG);
                        break;
                }

                $customField->setId($id);
                $customField->setName($field->name);
                $customField->setFieldType($field->field_type);
                $customField->setIsEditable($field->is_editable);
                $customField->setIsRequired($field->is_required);
                $customField->setIsDeletable($field->is_deletable);
                $customField->setIsVisible($field->is_visible);

                if (isset($field->enums)) {
                    $customField->setEnums((array) $field->enums);
                }

                $account->addCustomField($customField);
            }
        }
    }
}
