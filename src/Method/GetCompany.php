<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Company;
use Amocrm\ApiClient\Utils\Mapper;

class GetCompany extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/companies/', $params);

        $company = new Company();

        $data = $response->_embedded->items[0];

        $company->setId($data->id);
        $company->setName($data->name);
        $company->setResponsibleUserId($data->responsible_user_id);
        $company->setCreatedBy($data->created_by);
        $company->setCreatedAt($data->created_at);
        $company->setUpdatedAt($data->updated_at);
        $company->setContactsId($data->contacts->id);
        $company->setLeadsId($data->leads->id);
        $company->setCustomersId($data->customers->id);
        $company->setCustomFields((array) $data->custom_fields);

        return $company;
    }
}
