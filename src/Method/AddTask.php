<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Task;

class AddTask extends AbstractMethod
{
    public function __invoke(Task $task)
    {
        $response = $this->client->post('/api/v2/tasks/', ['add' => [$task->getArrayCopy()]]);

        $task->setId($response->_embedded->items[0]->id);
    }
}
