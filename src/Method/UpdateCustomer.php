<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Customer;

class UpdateCustomer extends AbstractMethod
{
    public function __invoke(Customer $customer, array $unlink = [])
    {
        $data = [];

        if (!empty($unlink)) {
            $data['unlink'] = $unlink;

            if (isset($unlink['contacts_id']) &&
                is_array($unlink['contacts_id'])) {
                $customer->setContactsId(array_diff($customer->getContactsId(), $unlink['contacts_id'] ?? []));
            }

            if (isset($unlink['company_id'])) {
                $customer->setCompanyId(null);
            }
        }

        $lead->setUpdatedAt(time());
        $data = array_merge($lead->getArrayCopy(), $data);

        $this->client->post('/api/v2/customers/', ['update' => [$data]]);
    }
}
