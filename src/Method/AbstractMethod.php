<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;

class AbstractMethod
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
