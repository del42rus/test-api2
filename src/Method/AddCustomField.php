<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\CustomField;

class AddCustomField extends AbstractMethod
{
    public function __invoke(CustomField $customField)
    {
        $response = $this->client->post('/api/v2/fields/', ['add' => [$customField->getArrayCopy()]]);

        $customField->setId($response->_embedded->items[0]->id);
    }
}
