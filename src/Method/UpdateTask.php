<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Task;

class UpdateTask extends AbstractMethod
{
    public function __invoke(Task $task)
    {
        $task->setUpdatedAt(time());
        
        $response = $this->client->post('/api/v2/tasks/', ['update' => [$task->getArrayCopy()]]);
    }
}
