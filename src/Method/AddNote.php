<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Note;

class AddNote extends AbstractMethod
{
    public function __invoke(Note $note)
    {
        $response = $this->client->post('/api/v2/notes/', ['add' => [$note->getArrayCopy()]]);

        $note->setId($response->_embedded->items[0]->id);
    }
}
