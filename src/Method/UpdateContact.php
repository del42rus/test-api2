<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Contact;

class UpdateContact extends AbstractMethod
{
    public function __invoke(Contact $contact, array $unlink = [])
    {
        $data = [];

        if (!empty($unlink)) {
            $data['unlink'] = $unlink;

            if (isset($unlink['leads_id']) &&
                is_array($unlink['leads_id'])) {
                $contact->setLeadsId(array_diff($contact->getLeadsId(), $unlink['leads_id']));
            }

            if (isset($unlink['customers_id']) &&
                is_array($unlink['customers_id'])) {
                $contact->setCustomersId(array_diff($contact->getCustomersId(), $unlink['customers_id']));
            }

            if ($unlink['company_id']) {
                $contact->setCompanyId(null);
            }
        }

        $contact->setUpdatedAt(time());
        $data = array_merge($this->hydrator->extract($contact), $data);

        $this->client->post('/api/v2/contacts/', ['update' => [$data]]);
    }
}
