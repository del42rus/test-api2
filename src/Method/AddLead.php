<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Lead;

class AddLead extends AbstractMethod
{
    public function __invoke(Lead $lead)
    {
        $response = $this->client->post('/api/v2/leads/', ['add' => [$lead->getArrayCopy()]]);

        $lead->setId($response->_embedded->items[0]->id);
    }
}
