<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Company;

class UpdateCompany extends AbstractMethod
{
    public function __invoke(Company $company, array $unlink = [])
    {
        $data = [];

        if (!empty($unlink)) {
            $data['unlink'] = $unlink;

            if (isset($unlink['contacts_id']) &&
                is_array($unlink['contacts_id'])) {
                $company->setContactsId(array_diff($company->getContactsId(), $unlink['contacts_id'] ?? []));
            }

            if (isset($unlink['leads_id']) &&
                is_array($unlink['leads_id'])) {
                $company->setLeadsId(array_diff($company->getLeadsId(), $unlink['leads_id'] ?? []));
            }

            if (isset($unlink['customers_id']) &&
                is_array($unlink['customers_id'])) {
                $company->setCustomersId(array_diff($company->getCustomersId(), $unlink['customers_id'] ?? []));
            }
        }

        $company->setUpdatedAt(time());
        $data = array_merge($company->getArrayCopy(), $data);

        $this->client->post('/api/v2/companies/', ['update' => [$data]]);
    }
}
