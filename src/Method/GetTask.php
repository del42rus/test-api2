<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Task;
use Amocrm\ApiClient\Utils\Mapper;

class GetTask extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/tasks/', $params);

        $task = new Task();

        $data = $response->_embedded->items[0];

        $task->setId($data->id);
        $task->setName($data->name);
        $task->setResponsibleUserId($data->responsible_user_id);
        $task->setCreatedBy($data->created_by);
        $task->setCreatedAt($data->created_at);
        $task->setTaskType($data->task_type);
        $task->setElementId($data->element_id);
        $task->setElementType($data->element_type);
        $task->setText($data->text);
        $task->setCompleteTillAt($data->complete_till_at);
        $task->setIsCompleted($data->is_completed);

        return $note;
    }
}
