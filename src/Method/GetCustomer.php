<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Customer;
use Amocrm\ApiClient\Utils\Mapper;

class GetCustomer extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/customers/', $params);

        $customer = new Customer();

        $data = $response->_embedded->items[0];

        $customer->setId($data->id);
        $customer->setName($data->name);
        $customer->setResponsibleUserId($data->responsible_user_id);
        $customer->setCreatedBy($data->created_by);
        $customer->setNextDate($data->next_date);
        $customer->setPeriodicity($data->periodicity);
        $customer->setCompanyId($data->company->id ?? null);
        $customer->setCreatedAt($data->created_at);
        $customer->setUpdatedAt($data->updated_at);
        $customer->setContactsId($data->contacts->id);
        $customer->setCustomFields((array) $data->custom_fields);

        return $customer;
    }
}
