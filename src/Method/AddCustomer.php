<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Customer;

class AddCustomer extends AbstractMethod
{
    public function __invoke(Customer $customer)
    {
        $response = $this->client->post('/api/v2/customers/', ['add' => [$customer->getArrayCopy()]]);

        $customer->setId($response->_embedded->items[0]->id);
    }
}
