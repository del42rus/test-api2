<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Contact;
use Amocrm\ApiClient\Utils\Mapper;

class GetContact extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/contacts/', $params);

        $contact = new Contact();

        $data = $response->_embedded->items[0];

        $contact->setId($data->id);
        $contact->setName($data->name);
        $contact->setResponsibleUserId($data->responsible_user_id);
        $contact->setCreatedBy($data->created_by);
        $contact->setCreatedAt($data->created_at);
        $contact->setUpdatedAt($data->updated_at);
        $contact->setLeadsId($data->leads->id);
        $contact->setCompanyId($data->company->id ?? null);
        $contact->setCustomFields((array) $data->custom_fields);

        return $contact;
    }
}
