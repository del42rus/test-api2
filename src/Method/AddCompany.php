<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Company;

class AddCompany extends AbstractMethod
{
    public function __invoke(Company $company)
    {
        $data[] = $company->getArrayCopy();

        $response = $this->client->post('/api/v2/companies/', ['add' => [$company->getArrayCopy()]]);

        $company->setId($response->_embedded->items[0]->id);
    }
}
