<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Lead;

class UpdateLead extends AbstractMethod
{
    public function __invoke(Lead $lead, array $unlink = [])
    {
        $data = [];

        if (!empty($unlink)) {
            $data['unlink'] = $unlink;

            if (isset($unlink['contacts_id']) &&
                is_array($unlink['contacts_id'])) {
                $lead->setContactsId(array_diff($lead->getContactsId(), $unlink['contacts_id'] ?? []));
            }

            if (isset($unlink['company_id'])) {
                $lead->setCompanyId(null);
            }
        }

        $lead->setUpdatedAt(time());
        $data = array_merge($lead->getArrayCopy(), $data);

        $this->client->post('/api/v2/leads/', ['update' => [$data]]);
    }
}
