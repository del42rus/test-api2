<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Note;
use Amocrm\ApiClient\Utils\Mapper;

class GetNote extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/notes/', $params);

        $note = new Note();

        $data = $response->_embedded->items[0];

        $note->setId($data->id);
        $note->setName($data->name);
        $note->setResponsibleUserId($data->responsible_user_id);
        $note->setCreatedAt($data->created_at);
        $note->setUpdatedAt($data->updated_at);
        $note->setNoteType($data->note_type);
        $note->setElementId($data->element_id);
        $note->setElementType($data->element_type);
        $note->setText($data->text);
        $note->setParams($data->params);

        return $note;
    }
}
