<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Contact;

class AddContact extends AbstractMethod
{
    public function __invoke(Contact $contact)
    {
        $response = $this->client->post('/api/v2/contacts/', ['add' => [$contact->getArrayCopy()]]);

        $contact->setId($response->_embedded->items[0]->id);
    }
}
