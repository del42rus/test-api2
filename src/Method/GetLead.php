<?php

namespace Amocrm\ApiClient\Method;

use Amocrm\ApiClient\Client;
use Amocrm\ApiClient\Entity\Lead;
use Amocrm\ApiClient\Utils\Mapper;

class GetLead extends AbstractMethod
{
    public function __invoke($params)
    {
        $response = $this->client->get('/api/v2/leads/', $params);

        $lead = new Lead();

        $data = $response->_embedded->items[0];

        $lead->setId($data->id);
        $lead->setName($data->name);
        $lead->setResponsibleUserId($data->responsible_user_id);
        $lead->setCreatedAt($data->created_at);
        $lead->setUpdatedAt($data->updated_at);
        $lead->setContactsId($data->contacts->id);
        $lead->setCustomFields((array) $data->custom_fields);

        return $lead;
    }
}
