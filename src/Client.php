<?php

namespace Amocrm\ApiClient;

use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Client
{
    const AUTH_COOKIE_NAME = 'session_id';

    private $httpClient;

    private $endpoint;

    private $userLogin;

    private $userHash;

    private $useSessionId = true;

    private $sessionKeyFile = '/data/api_session_key.txt';

    private $lastRequestTime;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function get($resource, $params = [])
    {
        return $this->request($resource, $params);
    }

    public function post($resource, $data = [])
    {
        return $this->request($resource, $data, Request::METHOD_POST);
    }

    public function request($resource, $params = [], $method = Request::METHOD_GET)
    {
        $this->delay();

        if (!$this->isUseSessionId() || !$this->hasSessionId()) {
            $this->auth();
        }

        $this->httpClient->addCookie(self::AUTH_COOKIE_NAME, $this->getSessionId());

        $uri = $this->endpoint . $resource;

        if (count($params) && $method == Request::METHOD_GET) {
            $uri .= '?' . http_build_query($params);
        }

        $this->httpClient->setUri($uri);
        $this->httpClient->setMethod($method);
        $this->httpClient->getRequest()->setContent(null);

        if (count($params) && $method == Request::METHOD_POST) {
            $this->httpClient->getRequest()->setContent(json_encode($params));
            $this->httpClient->getRequest()->getHeaders()->addHeaders([
                'Content-Type: application/json',
            ]);
        }

        $response = $this->httpClient->send();

        // Сессия истекла, авторизуемся по новой
        if ($this->isUseSessionId() && $response->getStatusCode() == Response::STATUS_CODE_401) {
            $this->setUseSessionId(false);

            return $this->request($resource, $params, $method);
        }

        if (!$response->isSuccess()) {
            $body = json_decode($response->getBody());

            throw new \Exception(
                'AmoCRM Api error: '
                . ($body->detail ?? '') . PHP_EOL
                . $response->getStatusCode()
                . ' ' . $response->getReasonPhrase()
                . ': ' . $this->endpoint . $resource . ' Params: ' . print_r($params, true),
                $response->getStatusCode()
            );
        }

        $response = json_decode($response->getBody());

        if (is_null($response)) {
            throw new \Exception('AmoCRM error: cannot parse json. ' . json_last_error());
        }

        return $response;
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getUserLogin()
    {
        return $this->userLogin;
    }

    public function setUserLogin($userLogin)
    {
        $this->userLogin = $userLogin;
    }

    public function getUserHash()
    {
        return $this->userHash;
    }

    public function setUserHash($userHash)
    {
        $this->userHash = $userHash;
    }

    protected function auth()
    {
        $this->httpClient->setUri($this->endpoint . '/private/api/auth.php?type=json');
        $this->httpClient->setMethod('POST');

        $params = [
            'USER_LOGIN' => $this->userLogin,
            'USER_HASH' => $this->userHash
        ];

        $this->httpClient->getRequest()->setContent(json_encode($params));

        $this->httpClient->getRequest()->getHeaders()->addHeaders([
            'Content-Type: application/json',
        ]);

        $response = $this->httpClient->send();

        $sessionId = $this->getSessionIdFromCookie($response);
        $this->setSessionId($sessionId);
    }

    private function getSessionIdFromCookie(Response $response)
    {
        foreach ($response->getCookie() as $cookie) {
            if ($cookie->getName() == self::AUTH_COOKIE_NAME) {
                return $cookie->getValue();
            }
        }

        return null;
    }

    private function setSessionId($sessionId)
    {
        @mkdir(getcwd() . dirname($this->sessionKeyFile));

        file_put_contents(getcwd() . $this->sessionKeyFile, $sessionId);
    }

    private function hasSessionId()
    {
        if (!file_exists(getcwd() . $this->sessionKeyFile)) {
            return false;
        }

        return !empty(file_get_contents(getcwd() . $this->sessionKeyFile));
    }

    private function getSessionId()
    {
        if (!file_exists(getcwd() . $this->sessionKeyFile)) {
            return false;
        }
        
        return file_get_contents(getcwd() . $this->sessionKeyFile);
    }

    private function isUseSessionId()
    {
        return $this->useSessionId;
    }

    private function setUseSessionId($useSessionId)
    {
        $this->useSessionId = $useSessionId;
    }

    public function __call($name, $arguments)
    {
        $methodClass = __NAMESPACE__ . '\\Method\\' . ucfirst($name);

        $method = new $methodClass($this);

        return call_user_func_array($method, $arguments);
    }

    private function delay()
    {
        if ($this->lastRequestTime) {
            return;
        }

        $time = microtime(true) - $this->lastRequestTime;

        if ($time < 1) {
            $delay = ceil((1 - $time) * 1000000);
            usleep($delay);
        }

        $this->lastRequestTime = microtime(true);
    }
}
