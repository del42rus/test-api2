<?php

namespace Amocrm\ApiClient;

use Zend\Http\Client as HttpClient;
use Zend\Http\Client\Adapter\Curl;

class ClientFactory
{
    public static function create($config)
    {
        if (empty($config['endpoint'])) {
            throw new \Exception('Api endpoint is not provided');
        }

        if (empty($config['user_login'])) {
            throw new \Exception('Login is not provided');
        }

        if (empty($config['user_hash'])) {
            throw new \Exception('User key is not provided');
        }

        $httpClient = new HttpClient();
        $httpClient->setOptions([
            'timeout' => 120,
            'adapter' => Curl::class,
            'curloptions' => [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT => 'amoCRM-API-client/1.0',
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0,
            ],
        ]);

        $client = new Client($httpClient);
        $client->setEndpoint($config['endpoint']);
        $client->setUserLogin($config['user_login']);
        $client->setUserHash($config['user_hash']);

        return $client;
    }
}
